#!/usr/bin/env julia

"""
EEG-ADC Analog-Digital Converter and Analyzer for scanned EEG recordings

Image properties:
100 DPI
490 x 100 px
H 2.5 cm
W 12.5 cm
"""

if VERSION < v"1.7.0"
    @error("This version of NeuroAnalyzer requires Julia 1.7.0 or above.")
end

program_version = "1.0"
println()
println("EEG-ADC analyzer v$program_version")
println("=====================")
println()

@info "Loading modules"
using CSV
using DataFrames
using DSP
using FFTW
using FileIO
using Images
using ImageBinarization
using ImageFiltering
using ImageMorphology
using ImageView
using Interpolations
using Pkg
using Plots
using Simpson
using Statistics

FFTW.set_num_threads(Sys.CPU_THREADS)

#=
m = Pkg.Operations.Context().env.manifest
println("Imported packages:")
println("              CSV $(m[findfirst(v -> v.name == "CSV", m)].version)")
println("       DataFrames $(m[findfirst(v -> v.name == "DataFrames", m)].version)")
println("              DSP $(m[findfirst(v -> v.name == "DSP", m)].version)")
println("           FileIO $(m[findfirst(v -> v.name == "FileIO", m)].version)")
println("           Images $(m[findfirst(v -> v.name == "Images", m)].version)")
println("ImageBinarization $(m[findfirst(v -> v.name == "ImageBinarization", m)].version)")
println("   ImageFiltering $(m[findfirst(v -> v.name == "ImageFiltering", m)].version)")
println("  ImageMorphology $(m[findfirst(v -> v.name == "ImageMorphology", m)].version)")
println("             FFTW $(m[findfirst(v -> v.name == "FFTW", m)].version)")
println("            Plots $(m[findfirst(v -> v.name == "Plots", m)].version)")
println("          Simpson $(m[findfirst(v -> v.name == "Simpson", m)].version)")
println("    NeuroAnalyzer $(m[findfirst(v -> v.name == "NeuroAnalyzer", m)].version)")
println()
=#

length(ARGS) == 0 && throw(ArgumentError("No input file provided!"))

dpi = 100

for args_idx in ARGS
    in_filename = args_idx

    @assert isfile(in_filename) "File $in_filename does not exist!"

    # load data
    @info "Loading data"

    println("Reading input image: $in_filename")
    img = Gray.(load(in_filename))
    dimy = size(img, 1)
    dimx = size(img, 2)

    @info "Converting"

    # adjust histogram
    # in some cases modify the `nbins` parameter for better results 
    # alg = Equalization(nbins = 3)
    # img = adjust_histogram(img, alg)

    # denoise image with median filter
    img = mapwindow(median, img, (3, 3))

    # unsharp mask
    # img = unsharp_mask(img, radius=5, amount=2)

    # edge detection
    # img = imedge(img, KernelFactors.sobel)[3]
    img = imedge(img, KernelFactors.sobel)[3]

    # binarize
    img_bin = binarize(img, Otsu())
    # img_bin = binarize(img, MinimumError())
    img_bin = dilate(img_bin)
    # img_bin = Bool.(img_bin)
    img_bin = Int.(img_bin)
    # fill in discontinuities
    for idx in 2:dimx
        c = img_bin[:, idx]
        sum(c) == 0 && (img_bin[:, idx] = img_bin[:, idx - 1])
    end
    # thinning
    for idx in 1:dimx
        c = img_bin[:, idx]
        l = sum(c) ÷ 2
        l_idx = findfirst(isequal(1), c)
        img_bin[:, idx] .= 0
        l_idx !== nothing && (img_bin[l_idx + l, idx] = 1)
    end

    # interpolate
    # TODO: input argument for user-defined px_uv and px_s
    t = 1:dimx
    signal = zeros(Int64, dimx)
    for idx in 1:dimx
        signal[idx] = dimy - findfirst(isequal(1), img_bin[:, idx])
    end
    s = round.(Int64, CubicSplineInterpolation(t, signal))
    img_bin = zeros(Int64, size(img_bin))
    for idx in 1:dimx
        img_bin[(dimy - s[idx]), idx] = 1
    end

    # digitize: convert to EEG signal
    px_cm = 0.03937008 * dpi * 10      # 1 dpi = 0.03937008 px/mm => px/mm => px/cm
    px_uv = 200 / px_cm                # 1 cm = 200 uV => convert to pixels
    px_s = 0.00025 * px_cm             # 2.5 cm = 1 s => convert to pixels
    fs = dpi
    eeg_signal = zeros(dimx)
    eeg_time = zeros(dimx)
    for idx in 1:dimx
        eeg_signal[idx] = px_uv * ((dimy ÷ 2) - findfirst(isequal(1), img_bin[:, idx]))
        eeg_time[idx] = idx * px_s
    end
    eeg_time = round.(eeg_time, digits=2)
    eeg_signal = round.(eeg_signal, digits=1)

    # FFT
    @info "Calculating FFT"

    N = length(eeg_signal)
    eeg_fft = fft(eeg_signal)
    eeg_fft_amp = 2 .* abs.(eeg_fft) ./ N
    eeg_fft_amp[1] = 0
    hz = collect(range(0, fs ÷ 2, (N ÷ 2) + 1))

    # Power Spectrum Density
    @info "Calculating PSD"

    psd = welch_pgram(eeg_signal, 4*fs, fs=fs)
    psd_freq = Vector(psd.freq)
    freq_res = psd.freq[2] - psd.freq[1]
    psd_power = psd.power
    psd_db = pow2db.(power(psd))

    vsearch(y::Real, x::AbstractVector) = findmin(abs.(x .- y))[2]

    function band_power(psd_power::AbstractVector, psd_freq::AbstractVector, dx::Float64, f::Tuple{Real, Real})
        f[1] < 0 && throw(ArgumentError("Lower frequency bound must be ≥ 0.")) 
        f[2] > fs / 2 && throw(ArgumentError("Lower frequency bound must be ≤ $(fs / 2).")) 
        f1_idx = vsearch(f[1], psd_freq)
        f2_idx = vsearch(f[2], psd_freq)
        frq_idx = [f1_idx, f2_idx]
        return simpson(psd_power[frq_idx[1]:frq_idx[2]], psd_freq[frq_idx[1]:frq_idx[2]], dx=dx)
    end

    # compute the absolute power by approximating the area under the curve
    total_power = round(simpson(psd_power, dx=freq_res), digits=3)
    abs_delta = round(band_power(psd_power, psd_freq, freq_res, (0, 4)), digits=3)
    abs_theta = round(band_power(psd_power, psd_freq, freq_res, (4, 8)), digits=3)
    abs_alpha = round(band_power(psd_power, psd_freq, freq_res, (8, 13)), digits=3)
    abs_alpha1 = round(band_power(psd_power, psd_freq, freq_res, (8, 10)), digits=3)
    abs_alpha2 = round(band_power(psd_power, psd_freq, freq_res, (10, 13)), digits=3)
    abs_beta = round(band_power(psd_power, psd_freq, freq_res, (13, 31)), digits=3)
    abs_beta1 = round(band_power(psd_power, psd_freq, freq_res, (13, 18)), digits=3)
    abs_beta2 = round(band_power(psd_power, psd_freq, freq_res, (18, 31)), digits=3)
    abs_gamma = round(band_power(psd_power, psd_freq, freq_res, (31, hz[end])), digits=3)
    abs_gamma1 = round(band_power(psd_power, psd_freq, freq_res, (31, 41)), digits=3)
    abs_gamma2 = round(band_power(psd_power, psd_freq, freq_res, (41, hz[end])), digits=3)

    abs_delta_db = round(band_power(psd_db, psd_freq, freq_res, (0, 4)), digits=3)
    abs_theta_db = round(band_power(psd_db, psd_freq, freq_res, (4, 8)), digits=3)
    abs_alpha_db = round(band_power(psd_db, psd_freq, freq_res, (8, 13)), digits=3)
    abs_alpha1_db = round(band_power(psd_db, psd_freq, freq_res, (8, 10)), digits=3)
    abs_alpha2_db = round(band_power(psd_db, psd_freq, freq_res, (10, 13)), digits=3)
    abs_beta_db = round(band_power(psd_db, psd_freq, freq_res, (13, 31)), digits=3)
    abs_beta1_db = round(band_power(psd_db, psd_freq, freq_res, (13, 18)), digits=3)
    abs_beta2_db = round(band_power(psd_db, psd_freq, freq_res, (18, 31)), digits=3)
    abs_gamma_db = round(band_power(psd_db, psd_freq, freq_res, (31, hz[end])), digits=3)
    abs_gamma1_db = round(band_power(psd_db, psd_freq, freq_res, (31, 41)), digits=3)
    abs_gamma2_db = round(band_power(psd_db, psd_freq, freq_res, (41, hz[end])), digits=3)

    rel_delta = round(abs_delta / total_power, digits=3)
    rel_theta = round(abs_theta / total_power, digits=3)
    rel_alpha = round(abs_alpha / total_power, digits=3)
    rel_alpha1 = round(abs_alpha1 / total_power, digits=3)
    rel_alpha2 = round(abs_alpha2 / total_power, digits=3)
    rel_beta = round(abs_beta / total_power, digits=3)
    rel_beta1 = round(abs_beta1 / total_power, digits=3)
    rel_beta2 = round(abs_beta2 / total_power, digits=3)
    rel_gamma = round(abs_gamma / total_power, digits=3)
    rel_gamma1 = round(abs_gamma1 / total_power, digits=3)
    rel_gamma2 = round(abs_gamma2 / total_power, digits=3)

    # draw and save plots
    @info "Saving plots"

    # save binary image
    bw_filename = replace(in_filename, ".png" => "-bw.png")
    println("Saving PNG: $bw_filename")
    save(bw_filename, Gray.(img_bin))

    # plot signal
    pdf_filename = replace(in_filename, ".png" => "-signal.png")
    println("Saving PDF: $pdf_filename")
    p = plot(eeg_time,
             eeg_signal,
             legend=false,
             lw=1,
             xlim=(0, ceil(eeg_time[end])),
             ylim=(-250, 250),
             title="EEG signal",
             xlabel="Time [s]",
             ylabel="Voltage [μV]",
             grid=true,
             titlefontsize=10,
             guidefontsize=10,
             tickfontsize=6);
    savefig(p, pdf_filename)

    # plot FFT
    pdf_filename = replace(in_filename, ".png" => "-fft.png")
    println("Saving PDF: $pdf_filename")
    p = plot(hz,
             eeg_fft_amp[1:length(hz)],
             legend=false,
             lw=1,
             xlim=(0, maximum(hz)),
             ylim=(0, maximum(eeg_fft_amp[1:length(hz)] * 1.1)),
             title="FFT",
             xlabel="Frequency [Hz]",
             ylabel="2 × |FFT|",
             grid=true,
             titlefontsize=10,
             guidefontsize=10,
             tickfontsize=6);
    Plots.annotate!(2, maximum(eeg_fft_amp[1:length(hz)]) * 1.1, text("delta", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(6, maximum(eeg_fft_amp[1:length(hz)]) * 1.1, text("theta", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(10.5, maximum(eeg_fft_amp[1:length(hz)]) * 1.1, text("alpha", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(20.5, maximum(eeg_fft_amp[1:length(hz)]) * 1.1, text("beta", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(40, maximum(eeg_fft_amp[1:length(hz)]) * 1.1, text("gamma", pointsize=6, valign=:top, halign=:center))
    vline!([4], lc=:gray, ls=:dash)
    vline!([8], lc=:gray, ls=:dash)
    vline!([13], lc=:gray, ls=:dash)
    vline!([30], lc=:gray, ls=:dash)
    savefig(p, pdf_filename)

    # plot PSD
    pdf_filename = replace(in_filename, ".png" => "-psd.png")
    println("Saving PDF: $pdf_filename")
    p = plot(psd_freq,
             psd_power,
             legend=false,
             lw=1,
             xlim=(0, maximum(psd_freq)),
             ylim=(minimum(psd_power), maximum(psd_power) * 1.1),
             title="Welch's periodogram",
             xlabel="Frequency [Hz]",
             ylabel="Power spectral density [μV^2 / Hz]",
             grid=true,
             titlefontsize=10,
             guidefontsize=10,
             tickfontsize=6);
    Plots.annotate!(2, maximum(psd_power) * 1.1, text("delta", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(6, maximum(psd_power) * 1.1, text("theta", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(10.5, maximum(psd_power) * 1.1, text("alpha", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(20.5, maximum(psd_power) * 1.1, text("beta", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(40, maximum(psd_power) * 1.1, text("gamma", pointsize=6, valign=:top, halign=:center))
    vline!([4], lc=:gray, ls=:dash)
    vline!([8], lc=:gray, ls=:dash)
    vline!([13], lc=:gray, ls=:dash)
    vline!([30], lc=:gray, ls=:dash)
    savefig(p, pdf_filename)

    # plot PSD dB
    pdf_filename = replace(in_filename, ".png" => "-psd_db.png")
    println("Saving PDF: $pdf_filename")
    p = plot(psd_freq,
             psd_db,
             legend=false,
             lw=1,
             xlim=(0, maximum(psd_freq)),
             ylim=(minimum(psd_db), maximum(psd_db) * 1.1),
             title="Welch's periodogram",
             xlabel="Frequency [Hz]",
             ylabel="Power spectral density [dB]",
             grid=true,
             titlefontsize=10,
             guidefontsize=10,
             tickfontsize=6);
    Plots.annotate!(2, maximum(psd_db) * 1.1, text("delta", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(6, maximum(psd_db) * 1.1, text("theta", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(10.5, maximum(psd_db) * 1.1, text("alpha", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(20.5, maximum(psd_db) * 1.1, text("beta", pointsize=6, valign=:top, halign=:center))
    Plots.annotate!(40, maximum(psd_db) * 1.1, text("gamma", pointsize=6, valign=:top, halign=:center))
    vline!([4], lc=:gray, ls=:dash)
    vline!([8], lc=:gray, ls=:dash)
    vline!([13], lc=:gray, ls=:dash)
    vline!([30], lc=:gray, ls=:dash)
    savefig(p, pdf_filename)

    # plot absolute powers
    pdf_filename = replace(in_filename, ".png" => "-bands_abs.png")
    println("Saving PDF: $pdf_filename")
    abs_band_powers = [abs_delta, abs_theta, abs_alpha, abs_alpha1, abs_alpha2, abs_beta, abs_beta1, abs_beta2, abs_gamma, abs_gamma1, abs_gamma2]
    band_names = ["delta", "theta", "alpha", "alpha1", "alpha2", "beta", "beta1", "beta2", "gamma", "gamma1", "gamma2"]
    p = bar(abs_band_powers,
            xticks=(1:length(band_names), band_names),
            ylabel="Absolute power [μV^2 / Hz]",
            title="Absolute power across EEG bands",
            legend=false,
            xrotation = 90,
            titlefontsize=10,
            guidefontsize=10,
            tickfontsize=6);
    savefig(p, pdf_filename)

    # plot absolute powers dB
    pdf_filename = replace(in_filename, ".png" => "-bands_abs_db.png")
    println("Saving PDF: $pdf_filename")
    abs_band_powers_db = [abs_delta_db, abs_theta_db, abs_alpha_db, abs_alpha1_db, abs_alpha2_db, abs_beta_db, abs_beta1_db, abs_beta2_db, abs_gamma_db, abs_gamma1_db, abs_gamma2_db]
    band_names = ["delta", "theta", "alpha", "alpha1", "alpha2", "beta", "beta1", "beta2", "gamma", "gamma1", "gamma2"]
    p = bar(abs_band_powers_db,
            xticks=(1:length(band_names), band_names),
            ylabel="Absolute power [dB]",
            title="Absolute power across EEG bands",
            legend=false,
            xrotation = 90,
            titlefontsize=10,
            guidefontsize=10,
            tickfontsize=6);
    savefig(p, pdf_filename)

    # plot relative powers
    pdf_filename = replace(in_filename, ".png" => "-bands_rel.png")
    println("Saving PDF: $pdf_filename")
    rel_band_powers = [rel_delta, rel_theta, rel_alpha, rel_alpha1, rel_alpha2, rel_beta, rel_beta1, rel_beta2, rel_gamma, rel_gamma1, rel_gamma2]
    band_names = ["delta", "theta", "alpha", "alpha1", "alpha2", "beta", "beta1", "beta2", "gamma", "gamma1", "gamma2"]
    p = bar(rel_band_powers,
            xticks=(1:length(band_names), band_names),
            ylabel="Relative power",
            title="Relative power across EEG bands",
            legend=false,
            xrotation = 90,
            titlefontsize=10,
            guidefontsize=10,
            tickfontsize=6);
    savefig(p, pdf_filename)

    # save band data
    @info "Saving results"

    # save signal to CSV file
    df = DataFrame(:time => eeg_time, :signal => eeg_signal)
    csv_filename = replace(in_filename, ".png" => "-signal.csv")
    println("Saving CSV: $csv_filename")
    CSV.write(csv_filename, df)

    csv_filename = replace(in_filename, ".png" => "-bands.csv")
    println("Saving CSV: $csv_filename")
    band_names = ["total_power", "delta", "theta", "alpha", "alpha1", "alpha2", "beta", "beta1", "beta2", "gamma", "gamma1", "gamma2"]
    rel_band_powers = [total_power, rel_delta, rel_theta, rel_alpha, rel_alpha1, rel_alpha2, rel_beta, rel_beta1, rel_beta2, rel_gamma, rel_gamma1, rel_gamma2]
    abs_band_powers = [total_power, abs_delta, abs_theta, abs_alpha, abs_alpha1, abs_alpha2, abs_beta, abs_beta1, abs_beta2, abs_gamma, abs_gamma1, abs_gamma2]
    abs_band_powers_db = [total_power, abs_delta_db, abs_theta_db, abs_alpha_db, abs_alpha1_db, abs_alpha2_db, abs_beta_db, abs_beta1_db, abs_beta2_db, abs_gamma_db, abs_gamma1_db, abs_gamma2_db]
    df = DataFrame(:band => band_names, :abs_power => abs_band_powers, :abs_power_db => abs_band_powers_db, :rel_power => rel_band_powers)
    CSV.write(csv_filename, df)

    # write report
    txt_filename = replace(in_filename, ".png" => "-report.txt")
    println("Saving TXT: $txt_filename")
    open(txt_filename, "w") do f
        # header
        write(f, "Signal data\n")
        write(f, "===========\n")
        write(f, "     Input signal: $in_filename\n")
        write(f, "Signal length [s]: $(eeg_time[end])\n\n")

        # amplitude
        write(f, "Signal amplitude\n")
        write(f, "================\n")
        write(f, "   min amplitude [μV]: $(round(minimum(eeg_signal), digits=2))\n")
        write(f, "   max amplitude [μV]: $(round(maximum(eeg_signal), digits=2))\n")
        write(f, "  mean amplitude [μV]: $(round(mean(eeg_signal), digits=2))\n")
        write(f, "median amplitude [μV]: $(round(median(eeg_signal), digits=2))\n\n")

        # power in EEG ranges
        write(f, "Signal power\n")
        write(f, "============\n")
        write(f, "          total power [μV^2 / Hz]: $total_power\n\n")
        write(f, "   δ band (0-3 Hz) absolute power: $abs_delta\n")
        write(f, "         δ band absolute power dB: $abs_delta_db\n")
        write(f, "            δ band relative power: $rel_delta\n")
        write(f, "   θ band (4-7 Hz) absolute power: $abs_theta\n")
        write(f, "         θ band absolute power dB: $abs_theta_db\n")
        write(f, "            θ band relative power: $rel_theta\n")
        write(f, "  α band (8-12 Hz) absolute power: $abs_alpha\n")
        write(f, "         α band absolute power dB: $abs_alpha_db\n")
        write(f, "            α band relative power: $rel_alpha\n")
        write(f, "  α1 band (8-9 Hz) absolute power: $abs_alpha1\n")
        write(f, "        α1 band absolute power dB: $abs_alpha1_db\n")
        write(f, "           α1 band relative power: $rel_alpha1\n")
        write(f, "α2 band (10-12 Hz) absolute power: $abs_alpha2\n")
        write(f, "        α2 band absolute power dB: $abs_alpha2_db\n")
        write(f, "           α2 band relative power: $rel_alpha2\n")
        write(f, " β band (13-30 Hz) absolute power: $abs_beta\n")
        write(f, "         β band absolute power dB: $abs_beta_db\n")
        write(f, "            β band relative power: $rel_beta\n")
        write(f, "β1 band (13-17 Hz) absolute power: $abs_beta1\n")
        write(f, "        β1 band absolute power dB: $abs_beta1_db\n")
        write(f, "           β1 band relative power: $rel_beta1\n")
        write(f, "β2 band (18-30 Hz) absolute power: $abs_beta2\n")
        write(f, "        β2 band absolute power dB: $abs_beta2_db\n")
        write(f, "           β2 band relative power: $rel_beta2\n")
        write(f, " γ band (31-$(round(Int64, hz[end])) Hz) absolute power: $abs_gamma\n")
        write(f, "         γ band absolute power dB: $abs_gamma_db\n")
        write(f, "            γ band relative power: $rel_gamma\n")
        write(f, "γ1 band (31-40 Hz) absolute power: $abs_gamma1\n")
        write(f, "           γ1 band absolute power: $abs_gamma1_db\n")
        write(f, "           γ1 band relative power: $rel_gamma1\n")
        write(f, "γ2 band (42-$(round(Int64, hz[end])) Hz) absolute power: $abs_gamma2\n")
        write(f, "        γ2 band absolute power dB: $abs_gamma2_db\n")
        write(f, "           γ2 band relative power: $rel_gamma2\n")
        close(f)
    end
end

@info "Analysis completed"
println()