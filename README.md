# EEG_ADC.jl

This Julia script converts scanned EEG recording produced by ECT machines, e.g. Thymatron.

Digitized EEG signal is further analyzed: mainly spectrum and power spectrum are calculated for the whole signal and all EEG bands.

EEG_ADC was initially written in Python. Original repository is [here](https://notabug.org/AdamWysokinski/EEG_ADC). The Julia port utilizes slightly different conversion algorithm, but results are comparable. The Python version is no more maintained. Any bugs and issues should be submitted here.

## How to cite

```bibtex
@article{title = {EEG_ADC: Digitizer and Analyzer of Electroconvulsive Therapy Paper Electroencephalogram Recordings},
    issn = {1533-4112, 1095-0680},
    url = {https://journals.lww.com/10.1097/YCT.0000000000000850},
    doi = {10.1097/YCT.0000000000000850},
    journal = {The Journal of ECT},
    author = {Wysokiński, Adam},
    year = {2022}
}
```

## Installation

Download and unzip the repository from: https://codeberg.org/AdamWysokinski/EEG_ADC.jl/archive/main.zip or clone it using git:

    $ git clone https://codeberg.org/AdamWysokinski/EEG_ADC.jl

Julia ≥ 1.7.0 and the following packages are required:
- CSV
- DataFrames
- DSP
- FFTW
- FileIO
- Images
- ImageBinarization
- ImageFiltering
- ImageMorphology
- ImageView
- Interpolations
- Plots
- Simpson
- Statistics

## Usage

Example input file:
![example.png](https://codeberg.org/AdamWysokinski/EEG_ADC.jl/src/master/example/example.png)

The scanned input image (PNG preferably) has to be:
- DPI: 100
- resolution: 490×100 pixels
- dimensions on paper: height 2.5 cm, width 12.5 cm

The program tries to eliminate noise, background grid, etc. In order to get best results you may also manually remove them using any image editor, e.g. free and open-source [GIMP](https://www.gimp.org).

To analyze the scanned image, run the program as:

    $ ./eeg_adc.jl file_name.png

After processing, the following output files are produced:
1. binary (black and white) version of the input image: file_name-bw.png
2. digitized signal: file_name-signal.csv (with two columns: time and amplitude)
3. signal plot: file_name-signal.pdf
4. signal spectrum: file_name-fft.pdf
5. power spectrum: file_name-psd.pdf and file_name-psd_db.pdf
6. absolute and relative powers across EEG bands: file_name-bands.cvs
7. absolute and relative powers across EEG bands bar plot: file_name-bands_abs.pdf, file_name-bands_abs_db.pdf, file_name-bands_rel.pdf
8. summary report (signal filename, max, min, mean and median amplitudes, total power, absolute and relative powers): file_name-report.txt

Output files are stored in the same directory as the input file. **Important:** old output files are overwritten without prompting!

Below is a typical session:

    $ ./eeg_adc.jl example/example.png

    EEG-ADC analyzer v1.0
    =====================

    [ Info: Loading modules
    [ Info: Loading data
    Reading input image: example.png
    [ Info: Converting
    [ Info: Calculating FFT
    [ Info: Calculating PSD
    [ Info: Saving plots
    Saving PNG: example-bw.png
    Saving PDF: example-signal.pdf
    Saving PDF: example-fft.pdf
    Saving PDF: example-psd.pdf
    Saving PDF: example-psd_db.pdf
    Saving PDF: example-bands_abs.pdf
    Saving PDF: example-bands_abs_db.pdf
    Saving PDF: example-bands_rel.pdf
    [ Info: Saving results
    Saving CSV: example-signal.csv
    Saving CSV: example-bands.csv
    Saving TXT: example-report.txt
    [ Info: Analysis completed

## TODO

* input argument for user-defined px_uv and px_s (p1)
* input image of any dimensions and DPI (p2)

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request

## History

v1.0 (14/10/2022) First official release.

## Credits

Author: Prof. Adam Wysokiński (adam.wysokinski AT umed.lodz.pl)

Department of Old Age Psychiatry and Psychotic Disorders

Medical University of Lodz, Poland

## License

This software is licensed under [The 2-Clause BSD License](LICENSE).